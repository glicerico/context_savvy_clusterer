import unittest

from data_generator import RandomDataGenerator
from clusterer import CSClusterer
from numpy.testing import assert_allclose


def generate_and_cluster(generator):
    data = generator.generate_data()

    clusterer = CSClusterer(data)
    clusterer.fill_matrix()

    return clusterer


class ClustererCase(unittest.TestCase):
    """
    Tests for the Context-savvy clusterer
    """

    def test_fill_matrix(self):
        # Small corpus, to check by hand
        generator = RandomDataGenerator(num_words=4, num_inst=2, num_feat=3, num_ctx=2, rn_seed=211)
        clusterer = generate_and_cluster(generator)
        matrix = clusterer.M
        # pretty_words = json.dumps(clusterer.words, indent=2, sort_keys=True)
        # print(clusterer.contexts)
        # print(pretty_words)
        # print(matrix)
        self.assertIsNone(assert_allclose(matrix, [[0, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 0]]))

    def test_pipeline(self):
        generator = RandomDataGenerator(rn_seed=172)  # Use random seed for repeatability
        clusterer = generate_and_cluster(generator)
        generator.plot_data()
        matrix = clusterer.M

        self.assertIsNone(assert_allclose(matrix[0], [0, 0, 0, 0, 0, 0, 1, 1, 0, 0]))
        self.assertEqual(matrix[3][4], 1)

        clusterer.process_tsne()
        clusterer.cluster_fuzzy()
        clusterer.cluster_hard()


if __name__ == '__main__':
    unittest.main()
