import matplotlib.pyplot as plt
import numpy as np
import skfuzzy as fuzz
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from scipy.spatial import distance
from sklearn.manifold import TSNE
import seaborn as sns
import pandas as pd


def get_dist(curr_ctx, elem_ctx, metric='cosine'):
    if metric == 'cosine':
        return distance.cosine(curr_ctx, elem_ctx)
    else:
        print("Metric not implemented")
        return None


class CSClusterer:
    """
    Context-savvy Clusterer
    Groups together words whose instances have similar probabilities of belonging to the same feature
    in similar contexts.
    """

    def __init__(self, data, ctx_radius=0.1, prob_radius=0.1):
        """
        :param: data: tuple containing the 'words' dictionary and 'contexts' list
        :param ctx_radius: threshold distance between context vectors
        """
        self.words, self.contexts = data
        self.vocab_size = len(self.words.keys())
        self.M = np.zeros((self.vocab_size, self.vocab_size))
        self.ctx_radius = ctx_radius
        self.prob_radius = prob_radius
        self.feature_groups = {}
        self.clusters_fuzzy = None
        self.clusters_hard = None

        self.df = pd.DataFrame()

    def __add_feature(self, feat, instance_id):
        """
        Add id of given instance to features dictionary
        """
        self.feature_groups[feat].add(instance_id)

    def fill_matrix(self):
        """
        Fill the context-savvy matrix for the given data
        """
        # Traverse all instances of all words once
        for word_id, instances in self.words.items():
            for instance_id, instance in instances.items():
                # Get instance's values
                curr_feat = instance['feat']
                curr_ctx_id = instance['ctx']
                curr_ctx = self.contexts[curr_ctx_id]
                curr_prob = instance['prob']

                # If there's no set for the current feature, start it
                if curr_feat not in self.feature_groups:
                    self.feature_groups[curr_feat] = set()

                # Probe all instances in the set for the same feature
                for element_id in self.feature_groups[curr_feat]:
                    elem_word_id = element_id[0]
                    elem_instance_id = element_id[1]
                    elem_ctx_id = self.words[elem_word_id][elem_instance_id]['ctx']

                    # Choose indexes to fill the upper half of the symmetric matrix
                    index1 = min(word_id, elem_word_id)
                    index2 = max(word_id, elem_word_id)

                    # Jump to the next instance if this word pair is already marked
                    if self.M[index1, index2] == 1:
                        continue

                    # Calculate selection parameters
                    elem_prob = self.words[elem_word_id][elem_instance_id]['prob']
                    elem_ctx = self.contexts[elem_ctx_id]
                    curr_dist = get_dist(curr_ctx, elem_ctx)
                    prob_dist = np.abs(curr_prob - elem_prob)

                    # Mark this word-pair if similar context, probability and feature (above)
                    if curr_dist < self.ctx_radius and prob_dist < self.prob_radius:
                        self.M[index1, index2] = 1

                # Add the current instance to the corresponding feature's set
                self.__add_feature(curr_feat, (word_id, instance_id))

        # Make upper triangular matrix symmetric
        self.M = np.logical_or(self.M, self.M.transpose())
        self.M = self.M.astype(int)

    def cluster_fuzzy(self, max_clusters=10):
        """
        Clusters words based on the context-savvy matrix self.M, using the fuzzy FCM algorithm
        """
        fpcs = []
        best_fpc = 0
        best_clusters = None

        for i in range(2, max_clusters):
            ncenters = i
            cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(self.M.transpose(), ncenters, 2, error=0.005, maxiter=1000,
                                                             metric='cosine', init=None)
            fpcs.append(fpc)
            if fpc > best_fpc:
                best_fpc = fpc
                best_clusters = u

        self.clusters_fuzzy = np.argmax(best_clusters, axis=0)

        print(f"fpcs history: {fpcs}")
        print(f"best fuzzy clusters: {self.clusters_fuzzy}")

        self.plot_data(self.clusters_fuzzy)

    def cluster_hard(self, max_clusters=10):
        """
        Clusters words based on the context-savvy matrix self.M, using the fuzzy FCM algorithm
        """
        inertias = []
        best_inertia = 1e9

        for i in range(2, max_clusters):
            ncenters = i
        #clustering = DBSCAN(min_samples=2)
            clustering = KMeans(n_clusters=ncenters, random_state=0)
            clustering.fit(self.M)
            curr_inertia = clustering.inertia_
            inertias.append(curr_inertia)
            if clustering.inertia_ < best_inertia:
                best_inertia = curr_inertia
                self.clusters_hard = clustering.labels_

        print(f"inertia history: {inertias}")
        print(f"best hard clusters: {self.clusters_hard}")

        self.plot_data(self.clusters_hard)

    def process_tsne(self):
        # Run TSNE
        tsne = TSNE(n_components=2, verbose=1, random_state=123, perplexity=min(self.M.shape[0] - 1, 30))
        z = tsne.fit_transform(self.M)
        self.df["comp-1"] = z[:, 0]
        self.df["comp-2"] = z[:, 1]

    def plot_data(self, labels):
        unique_labels = set(labels)

        # Plot results
        self.df['labels'] = labels

        sns.scatterplot(x="comp-1", y="comp-2", hue=self.df.labels.tolist(),
                        palette=sns.color_palette("hls", len(unique_labels)),
                        data=self.df).set(title=f"Clustered data T-SNE projection {labels}")

        plt.show()
