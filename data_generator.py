import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from numpy import random as rn
from sklearn.manifold import TSNE



class RandomDataGenerator:
    """
    Creates random synthetic data to test the CSC
    """

    def __init__(self, num_words=10, num_inst=5, num_feat=4, num_ctx=5, dim_ctx=10, stddev_ctx=0.1, rn_seed=None):
        self.num_words = num_words
        self.num_inst = num_inst
        self.num_feat = num_feat
        self.num_ctx = num_ctx
        self.dim_ctx = dim_ctx
        self.stddev_ctx = stddev_ctx

        self.rng = rn.default_rng(seed=rn_seed)
        self.contexts = []
        self.words = {}
        # Lists for easier plotting
        self.wordIDs = []
        self.feats = []
        self.probs = []

    def __add_noise(self, orig_context):
        """
        Adds normal noise to an original context vector
        :param orig_context: The original context vector
        :return: Realistic context vector based on the original one
        """
        noise = self.rng.normal(scale=self.stddev_ctx)
        return orig_context + noise

    def generate_data(self):
        """
        Generates synthetic data
        :return: Synthetic data in dictionary of words, and contexts
        """
        # Print data params
        print(f"Generating {self.num_words * self.num_inst} word instances.")
        print(f"Using {self.num_ctx} original contexts (each of {self.dim_ctx} dims) and {self.num_feat} features")

        # Create random context vectors
        base_contexts = self.rng.random((self.num_ctx, self.dim_ctx)) - 0.5
        # Create list of features
        features = [i for i in range(self.num_feat)]

        counter = 0
        for i in range(self.num_words):
            for j in range(self.num_inst):
                if i not in self.words.keys():
                    self.words[i] = {}
                context_num = self.rng.integers(len(base_contexts))
                self.contexts.append(self.__add_noise(base_contexts[context_num]))
                curr_feat = self.rng.choice(features)
                curr_prob = self.rng.random()
                self.words[i][j] = {
                    'ctx': counter,
                    'feat': int(curr_feat),
                    'prob': float(curr_prob)
                }
                counter += 1
                # Fill lists for plotting
                self.wordIDs.append(i)  # Label each instance with current word ID
                self.feats.append(curr_feat)  # Label each instance with current feature
                self.probs.append(curr_prob)  # Label each instance with current prob

        return self.words, self.contexts

    def plot_data(self):
        """
        Plots the previously generated contexts using TSNE, using the word numbers as labels
        """
        # Check that data was previously generated
        if len(self.contexts) == 0:
            print("Please generate the data before plotting it\n")
            exit(1)

        # Run TSNE
        tsne = TSNE(n_components=2, verbose=1, random_state=123)
        z = tsne.fit_transform(np.array(self.contexts))

        # Plot results
        df = pd.DataFrame()
        df['wordIDs'] = self.wordIDs
        df["feats"] = self.feats
        df['probs'] = self.probs
        df["comp-1"] = z[:, 0]
        df["comp-2"] = z[:, 1]

        sns.scatterplot(x="comp-1", y="comp-2", hue=df.feats.tolist(),
                             # style=df.wordIDs.tolist(),
                             size=df.probs.tolist(),
                             palette=sns.color_palette("hls", self.num_feat),
                             data=df).set(title="Synthetic data T-SNE projection")

        def label_point(x, y, val, ax):
            a = pd.concat({'x': x, 'y': y, 'val': val}, axis=1)
            for i, point in a.iterrows():
                ax.text(point['x'] + .02, point['y'], str(point['val']))

        label_point(df['comp-1'], df['comp-2'], df['wordIDs'].astype(str), plt.gca())
        plt.show()


if __name__ == '__main__':
    generator = RandomDataGenerator(rn_seed=4111)
    generator.generate_data()
    generator.plot_data()
