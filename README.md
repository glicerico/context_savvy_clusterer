Directory for the Context-Savvy Clusterer files

About the Context-savvy clusterer, Ben [wrote](https://snet.slack.com/archives/C9ELRQF6J/p1663277320072669):

Context-Savvy Clusterer (CSC): This takes as input a set of words, each associated with a set of contextual features.  A contextual feature is of the form (v,f,p) where v is a vector characterizing a context, f is a discrete feature, and p is a probability value.   It forms clusters that group together words that assign similar probabilities to similar features in similar contexts.

Andres proposed:

--------------------------------------
Possible approach:

We want to fill a symmetric, binary square matrix with all vocab words in the rows and columns: the context-savvy matrix M_cs.
A cell (i, j) in M_cs contains a boolean value indicating if any pair of instances of words i and j with a similar context v (within radius r_threshold), with the same discrete feature f, have both a high probability p (higher than p_threshold).

M_cs offers vectors for each word in the vocabulary, coding their relationship with the rest of the words.
Non-exclusive clustering (either overlapping or fuzzy) could then be performed on these vectors to group together words that assign high probabilities to given features in similar contexts.

------------------------------------

Pseudo algorithm to construct M_cs:
1. Zero-initialize M_cs.
2. Eliminate all word instances from the vocabulary with probability lower than p_threshold (noisy categorization)
3. Group the remaining word instances with high p in sets according to their features f (s_f)
4. For each word_i in the vocabulary:
 4.1. For each instance_j of word_i (with context vector v_ij and feature k):
  4.1.1. For each word instance in s_k (with context vector v_lm, corresponding to word l):
   4.1.1.1. If distance_metric(v_ij, v_lm) < r_threshold, then M_cs(i, l) = 1

Ben replied: 
The pseudocode looks to make sense ... 
I am unsure how critical filtering out low-probability word-instances will be (or to put it differently, maybe the 
p_threshold should be very low...) ... but I don't have a lot of practical experience dealing with super-huge corpora 
so my intuition on this may not be worth so much...